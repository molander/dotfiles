# README #

+ pkg install ack vim-lite git gitflow py27-pygments tmux zsh
+ git clone https://jnix3@bitbucket.org/jnix3/dotfiles.git ~/dotfiles
+ sh ~/dotfiles/oh-my-install.sh
+ sh ~/dotfiles/dotfiles.sh

# Now set your git name and email, one last time #
It's currently set to Beastie Daemon / web@ixsystems.com

+ git config --global user.name "Your Name"
+ git config --global user.email "you@mail.com"

# Cheatsheet: #
+ in the prompt, type: 'n-' without quotes and hit [TAB]

+ This will show you a list of zsh commands you can run, for example, n-history, n-kill, and n-list

+ colorize /usr/local/etc/nginx/nginx.conf |less # will show less output in color