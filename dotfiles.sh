#!/bin/sh

# set exports
export REPO=$HOME/dotfiles

# backup original .vim/, .zshrc, .vimrc, .gitconfig"
echo "I. make sure this repo is in ~/dotfiles"
echo ""
echo "II. make scripts executable"
echo "chmod +x ~/dotfiles/oh-my-zsh.sh"
echo "chmod +x ~/dotfiles/dotfiles.sh"
echo "III. run Oh My ZSH! installer"
echo "	+  cd ~/dotfiles && ./oh-my-zsh.sh"
echo "IV. run iX SpecOps mods & tweaks installer"
echo "	+  cd ~/dotfiles && ./oh-my-zsh.sh"
echo "V. download then double-click to install, use in terminal \n
https://github.com/abertsch/Menlo-for-Powerline"

echo "VI. backup original dotfiles: .zshrc, .vimrc, .gitconfig"
mv $HOME/.vimrc $HOME/.dot-backups/.vimrc-$(date +%F-%s)
mv $HOME/.vim $HOME/.dot-backups/.vim-$(date +%F-%s)
mv $HOME/.zshrc $HOME/.dot-backups/.zshrc-$(date +%F-%s)
mv $HOME/.gitconfig $HOME/.dot-backups/.gitconfiig-$(date +%F-%s)
echo "VII. create new .vim dir"

echo "VIII. install plugins,colors,syntax to ~/.vim/[bundle,colors,syntax]"
zsh -c "mkdir -p $HOME/.vim/bundle $HOME/.vim/colors/ $HOME/.vim/syntax"
git clone https://github.com/gmarik/Vundle.vim $HOME/.vim/bundle/Vundle.vim
zsh -c "cp -R $REPO/.vim/* $HOME/.vim/"
zsh -c "cp $REPO/.vimrc $HOME/.vimrc"
zsh -c "cp $REPO/.oh-my-zsh/themes/* $HOME/.oh-my-zsh/themes"
zsh -c "cp $REPO/.zshrc $HOME/.zshrc"
zsh -c "cp $REPO/.gitconfig $HOME/.gitconfig"
vim +PluginInstall +qall

echo "IX. The HiveMind Welomes You"
echo "Prepare for assimilation."
zsh
source $HOME/.zshrc
echo "Complete!"
echo "hint: type the following, then hit TAB \n
> n- \n
# this will give you a list of ncurses interfaces for useful things"
