  #!/bin/sh
  export REPO=$HOME/dotfiles
  zsh -c "mkdir -p $HOME/.vim/bundle $HOME/.vim/colors/ $HOME/.vim/syntax"
  git clone https://github.com/gmarik/Vundle.vim $HOME/.vim/bundle/Vundle.vim
  zsh -c "cp -R $REPO/.vim/* $HOME/.vim/"
  zsh -c "cp $REPO/.oh-my-zsh/themes/* $HOME/.oh-my-zsh/themes"
  zsh -c "cp $REPO/.gitconfig $HOME/.gitconfig"
